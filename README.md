# vue-js-exam

Olivier PETIT

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

### Docker

```
docker build -t exam-vue-js
```

```
docker run -it -p 8080:8080 --rm --name dockerize-vuejs-app-1 exam-vue-js
```

CICD VAR : deploy token oHefkegrEyCb3wPH5vfr
See [Configuration Reference](https://cli.vuejs.org/config/).

tets
